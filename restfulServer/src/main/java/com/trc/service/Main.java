package com.trc.service;

import java.net.URI;
import java.util.ResourceBundle;

import javax.ws.rs.core.UriBuilder;

import org.glassfish.jersey.jdkhttp.JdkHttpServerFactory;
import org.glassfish.jersey.media.multipart.MultiPartFeature;
import org.glassfish.jersey.server.ResourceConfig;

/**
 * 
 * Using Jersey & pure JDK to Publish RESTful Service.
 * 
 * @author Wenyang, Kao
 *
 */
public class Main {

	public static void main(String[] args) {
		ResourceBundle resourceBundle = ResourceBundle.getBundle("config");
		int port = Integer.parseInt(resourceBundle.getString("servicePort"));
		String contextPath = resourceBundle.getString("contextPath");
		URI uri = UriBuilder.fromUri(contextPath).port(port).build();
		ResourceConfig resourceConfig = new ResourceConfig(MyResource.class, MultiPartFeature.class);
		JdkHttpServerFactory.createHttpServer(uri, resourceConfig);
		
		// Check on your browser, URL==> http://localhost:8099/restfulServer/AuditTrailReceiver/echo?input=HelloWorld
		// If it works fine, your will get label "HelloWorld".
	}

}
