package com.trc.service;

import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ResourceBundle;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.glassfish.jersey.media.multipart.FormDataContentDisposition;
import org.glassfish.jersey.media.multipart.FormDataParam;


/**
 * 
 * RESTful Service
 * 
 * @author Wenyang, Kao
 *
 */
@Path("/AuditTrailReceiver")
public class MyResource {

	public MyResource() {
		ResourceBundle resourceBundle = ResourceBundle.getBundle("config");
		String storePath = resourceBundle.getString("storePath");
		File dir = new File(storePath);

		if (!dir.exists()) {
			dir.mkdirs();
		}
	}

	/**
	 * 
	 * Just use for testing, what you input what you see
	 * 
	 * @param input
	 * @return
	 */
	@GET
	@Path("/echo")
	public String echo(@QueryParam("input") String input) {
		return input;
	}

	/**
	 * Receive file and store it to "storePath".
	 * 
	 * @param fileInputStream "the file to inputStream"
	 * @param fileMetaData "The information of the file"
	 * @return
	 * @throws Exception
	 */
	@POST
	@Path("/upload")
	@Consumes({ MediaType.MULTIPART_FORM_DATA })
	public Response upload(
			@FormDataParam("myFile") InputStream fileInputStream,
			@FormDataParam("myFile") FormDataContentDisposition fileMetaData) throws Exception {
		OutputStream buffer = null;

		try {
			ResourceBundle resourceBundle = ResourceBundle.getBundle("config");
			String auditTrailPath = resourceBundle.getString("storePath");
			File targetFile = new File(auditTrailPath, fileMetaData.getFileName());
			buffer = new FileOutputStream(targetFile);
			int nRead;
			byte[] data = new byte[16384];

			while ((nRead = fileInputStream.read(data, 0, data.length)) != -1) {
				buffer.write(data, 0, nRead);
			}

			buffer.flush();

		} finally {
			if (buffer != null) {
				buffer.close();
			}
		}

		return Response.ok("Data uploaded successfully !!").build();
	}

}
